import React, { forwardRef, useEffect } from 'react';
import { useSelector, useDispatch } from "react-redux";
import { getUsers } from './store/actions/users.actions';
import './App.css';
import MaterialTable from "material-table";
import {
  SearchIcon,
  EditIcon,
  DeleteIcon
} from "./components";
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Chip from '@material-ui/core/Chip';
import { ArrowUpward, FirstPage, LastPage, ChevronRight, ChevronLeft } from '@material-ui/icons';

const App: React.FunctionComponent = () => {

  const users = useSelector((state: any) => state.users);
  //console.log(users);
  const dispatch = useDispatch();

  useEffect(() => {
    async function fetchData() {
      await dispatch(getUsers());
    }
    fetchData();
  }, [dispatch]);

  return (
    <Container className="App">
      <div className="header">
        <h4>Account users</h4>
        <Button className="connect-users-btn" >Connect users</Button>
      </div>
      <div className="table-container">
        <MaterialTable
          title=" "
          icons={{
            Edit: forwardRef((Props) => <EditIcon {...Props} />),
            Search: forwardRef((Props) => <SearchIcon  {...Props} />),
            Delete: forwardRef((Props) => <DeleteIcon {...Props} />),
            SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
            FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
            LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
            NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
            PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />)
          }}
          columns={[
            {
              title: 'User', field: 'user',
              render: (rowData: any) => {
                // console.log(rowData);
                return (
                  <div className="user-card">
                    <div className="img-contianer">
                      <Avatar alt={rowData.name} src={rowData.avatar} />
                    </div>
                    <div className="data-container">
                      <div className="name">{rowData.name}</div>
                      <div className="email">{rowData.email}</div>
                    </div>
                  </div>
                );
              },
            },
            {
              title: 'Permission', field: 'role',
              render: (rowData: any) => {
                if (rowData.role === 'ADMIN') {
                  return (
                    <Chip className="custom-chips adminChip" label={rowData.role} />
                  );
                }
                else if (rowData.role === 'AGENT') {
                  return (
                    <Chip className="custom-chips agentChip" label={rowData.role} />
                  );
                }
                else if (rowData.role === 'ACCOUNT_MANAGER') {
                  return (
                    <Chip className="custom-chips accountMChip" label={rowData.role} />
                  );
                }
                else if (rowData.role === 'EXTERNAL_REVIEWER') {
                  return (
                    <Chip className="custom-chips externalRChip" label={rowData.role} />
                  );
                }
              },
            }

          ]}

          data={users}

          options={{
            search: true,
            selection: true,
            actionsColumnIndex: -1,
            searchFieldStyle: {
              backgroundColor: '#fff',
              color: '#A0AEC0',
              border: "1px solid #E2E8F0",
              boxSizing: "border-box",
              borderRadius: "4px",
              width: "12.75rem",
              height: "2.5rem",
              padding: "0rem 0.3125rem"
            }
          }}

          actions={[
            {
              icon: 'edit',
              tooltip: 'Edit User',
              onClick: (event) => alert("EDIT User ")
            },

            {
              icon: 'delete',
              tooltip: 'Delete User',
              onClick: (event) => alert("Delete User")
            },
          ]}

          components={{
            Action: props => (
              <>
                {(props.action.icon === 'edit') && <Button onClick={(event) => props.action.onClick(event, props.data)} color="default" variant="contained" size="small" className="action-btn edit-btn" > <EditIcon /> Edit</Button>}
                {(props.action.icon === 'delete') && <Button onClick={(event) => props.action.onClick(event, props.data)} color="default" variant="contained" size="small" className="action-btn delete-btn" ><DeleteIcon />Delete</Button>}
              </>
            ),

          }}
        />
      </div>
    </Container>
  );
}

export default App;
